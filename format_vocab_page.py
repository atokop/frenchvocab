import sys
if len(sys.argv) != 2:
  print "ERROR: invalid number of arguments"
  print "Usage: python align.py [filename]"
  sys.exit()

input_filename = sys.argv[1]
infile = open(input_filename, 'rw')
delimiter = '----'
input_lines = []
output_lines = []

line_before_delimiter = True
for line in infile:
  if line.find(delimiter) == 0:
    line_before_delimiter = False
  elif line_before_delimiter:
    input_lines.append(line.strip())
  else:
    output_lines.append(line.strip())

combined_lines = []
for i in range(len(input_lines)):
  combined_lines.append(input_lines[i] + '|' + output_lines[i])

for line in combined_lines:
  print line
