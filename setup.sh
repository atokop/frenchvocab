[ "$UID" -eq 0 ] || exec sudo bash "$0" "$@"
curl https://bootstrap.pypa.io/get-pip.py -o - | python
pip install virtualenv
virtualenv env
source env/bin/activate
pip install -r requirements.txt
chmod a+x run.sh
ln -s $PWD/run.sh /usr/local/bin/kwafrench
