à peu près|about / roughly
à point|just right / medium
absolument|absolutely
d'accord|agreed / OK
alors|then
approprié|appropriate
au revoir|goodbye
aussi / aussi bien que|also / as well as
autant|as much
autre|other
autrefois|formerly
autrement|otherwise
avis|opinion / notice
bien entendu|of course
bien sûr|of course
bienvenue|welcome
bon|good
cela m'est égal !|I don't care!
cela ne fait rien|it doesn't matter
cependant|however
certain|certain / sure
chance|luck
chose|thing
de la part de|from (a person)
demi|half
donc|so / therefore
douzaine|dozen
égal|equal
également|equally / also, too
encore|still / yet
encore une fois|again
espace|space
espèce|sort / kind
exprès|on purpose
fois|time
grand’chose|much
idée|idea
il y a|there is / are
journée|day
machin|thing / whatsit
manche|handle
manière|manner / way
marque|make / brand
même|even / same
merci|thank you
milieu|middle
mixte|mixed
moins (que)|less (than) / minus
mois|month
moitié|half
moyen|medium / average
moyen|means / way
neuf|(brand) new
non plus|neither
nouveau|new
