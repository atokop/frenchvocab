import os, sys, random, subprocess, argparse
import requests
from gtts import gTTS

parser = argparse.ArgumentParser()
parser.add_argument("-s", "--start-word", help="the line number in vocab textfile to start on", default="0")
parser.add_argument("-n", "--num-words", help="the number of words to test", default = "30")
parser.add_argument("filename")
args = parser.parse_args()

start_word = int(args.start_word)
num_words = int(args.num_words)

resources_dir = os.path.dirname(os.path.realpath(__file__)) + "/res/"

requests.packages.urllib3.disable_warnings()

def makeDict(dictionary_file):
  dictionary = {}
  delimiter = '|'
  for line in readFile(resources_dir + dictionary_file + ".txt")[start_word:start_word + num_words]:
    if line[0] == '#':
      continue
    try:
      fr_word = line.split(delimiter)[0]
      eng_word = line.split(delimiter)[1]
      dictionary[eng_word] = fr_word
    except:
      pass
  return dictionary

# Returns array with each line as an entry from the file.
def readFile(filepath):
  f = open(filepath, 'r')
  return [line[:-1] for line in f]


def sayInFrench(target_text):
  audio_filename = "tmp/temp_audio.mp3"
  tts = gTTS(text=target_text, lang="fr")
  tts.save(audio_filename)
  subprocess.call(["afplay", audio_filename])


dictionary = makeDict(args.filename)
for key in dictionary:
  print("{: <40} {: <40}".format(key, dictionary[key]))
unused_words = dictionary.copy()
review_words = {}

while True:
  if len(unused_words.keys()) == 0:
    if len(review_words.keys()) != 0:
      print "Finished list: going to review words"
      unused_words = review_words.copy()
      review_words = {}
    else:
      print "Finished list! Restarting..."
      unused_words = dictionary.copy()
  key = random.choice(unused_words.keys())
  value = unused_words[key]
  unused_words.pop(key)
  print key
  if raw_input("") != "":
    print "saving word to review"
    review_words[key] = value
  print value
  try:
    sayInFrench(value)
  except Exception as e:
    print e
    print "Unable to play French dictation"
  if raw_input("") != "":
    print "saving word to review"
    review_words[key] = value
  os.system("clear")

